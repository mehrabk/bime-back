package com.bime.parsian.repository;

import com.bime.parsian.model.BimeSales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BimeSalesRepository extends JpaRepository<BimeSales, Long> {
}
