package com.bime.parsian.repository;

import com.bime.parsian.model.Ghest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GhestRepository extends JpaRepository<Ghest, Long> {
}