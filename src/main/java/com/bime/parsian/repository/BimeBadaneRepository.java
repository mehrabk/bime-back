package com.bime.parsian.repository;

import com.bime.parsian.model.BimeBadane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BimeBadaneRepository extends JpaRepository<BimeBadane, Long> {
}
