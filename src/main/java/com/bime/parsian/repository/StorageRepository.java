package com.bime.parsian.repository;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@Repository
public interface StorageRepository {
     void store(MultipartFile file, String fileName) throws IOException;
}
