package com.bime.parsian.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Ghest {

    public Ghest() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private int ghestNumber;

    @Getter
    @Setter
    private Date ghestDate;

    @Getter
    @Setter
    private long ghestPrice;

    @Getter
    @Setter
    @Lob
    private String imageUrl;

    @Getter
    @Setter
    private String smsStatus;

    @Getter
    @Setter
    @Lob
    private String note;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "bime_badane_id", referencedColumnName = "id")
    @JsonIgnore // ==> nemizare ke kolle object ba releationship ha ro response bede
    private BimeBadane bimeBadane;

    // Owner side
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "bime_sales_id", referencedColumnName = "id")
    @JsonIgnore
    private BimeSales bimeSales;

}
