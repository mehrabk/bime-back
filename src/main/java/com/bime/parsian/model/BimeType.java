package com.bime.parsian.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by Yoosef.
*/

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BimeType {
    TYPE_BIME_SALES(1), TYPE_BIME_BADANE(2);

    private final int id;

    BimeType(int id) {
        this.id = id;
        System.out.println(id);
    }

    public int getValue() {
        return id;
    }

    public String getLabel() {
        return toString();
    }

    public static BimeType getType(Integer id) {
        if(id == null) {
            return null;
        }
        for(BimeType item : BimeType.values()){
            if(id.equals(item.getValue())){
                return item;
            }
        }
        throw new IllegalArgumentException("No matching type for id " + id);
    }

    @Override
    public String toString() {
        switch (this) {
            case TYPE_BIME_SALES:
                return "بیمه ثالث";
            case TYPE_BIME_BADANE:
                return "بیمه بدنه";
            default:
                return "ERR";
        }
    }
}
