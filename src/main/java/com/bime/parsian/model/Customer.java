package com.bime.parsian.model;

import com.bime.parsian.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Customer extends DateAudit {

    public Customer() {
    }

    public Customer(String userName, String lastName, String address, String phoneNumber, String identityCode) {

        this.userName = userName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.identityCode = identityCode;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    private String userName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String phoneNumber;

    @Getter
    @Setter
    private String identityCode;



    @Getter
    @Setter
    @OneToMany(mappedBy = "customer", orphanRemoval=true, cascade = CascadeType.ALL)
    private List<BimeSales> bimeSalesList;

    @Getter
    @Setter
    @OneToMany(mappedBy = "customer", orphanRemoval=true, cascade = CascadeType.ALL)
    private List<BimeBadane> bimeBadaneList;





}

// JPA Relationships >> https://www.baeldung.com/
// Bidirectional Relationships >> https://docs.oracle.com/cd/E19798-01/821-1841/bnbqj/index.html
// https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch07.html#collections-bidirectional
