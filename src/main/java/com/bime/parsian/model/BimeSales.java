package com.bime.parsian.model;

import com.bime.parsian.model.Ghest;
import com.bime.parsian.model.Customer;
import com.bime.parsian.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BimeSales{

    public BimeSales() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private long bimeNumber;

    @Getter
    @Setter
    private long yektaCode;

    @Getter
    @Setter
    private long totalPrice;

    @Getter
    @Setter
    private long pishPardakht;

    private int type;

    @Getter
    @Setter
    @OneToMany(mappedBy = "bimeSales", cascade = CascadeType.ALL)
    private List<Ghest> ghestList = new ArrayList<>();

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @JsonIgnore
    private Customer customer;


    public BimeType getType() {
        System.out.println(BimeType.getType(type));
        return BimeType.getType(type);
    }
    public void setType(BimeType bimeType) {
        this.type = bimeType.getValue();
    }
}
