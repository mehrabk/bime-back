package com.bime.parsian.model.media;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Attachment {

    public Attachment(String source){
        this.source = source;
    }
    public Attachment() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    private String source;

    @Getter
    @Setter
    private int ghestNumber;

    // and other thind that you need


}
