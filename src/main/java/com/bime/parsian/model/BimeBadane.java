package com.bime.parsian.model;

import com.bime.parsian.model.Ghest;
import com.bime.parsian.model.Customer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class BimeBadane {

    public BimeBadane() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Long bimeNumber;;

    @Getter
    @Setter
    private Long yektaCode;

    @Getter
    @Setter
    private Long totalPrice;

    @Getter
    @Setter
    private Long pishPardakht;

    @Getter
    @Setter
    private String carName;

    private int type;

    @Getter
    @Setter
    @OneToMany(mappedBy = "bimeBadane", cascade = CascadeType.ALL)
    private List<Ghest> ghestList = new ArrayList<>();

    // non-owning side
    // The inverse side of a bidirectional relationship must refer to its owning side by using the mappedBy element.
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @JsonIgnore
    private Customer customer;


    public BimeType getType() {
        return BimeType.getType(type);
    }
    public void setType(BimeType bimeType) {
        this.type = bimeType.getValue();
    }

}


