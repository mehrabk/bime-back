package com.bime.parsian.service;

import com.bime.parsian.beans.MyBeanCopy;
import com.bime.parsian.model.BimeBadane;
import com.bime.parsian.repository.BimeBadaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

@Service
public class BimeBadaneService {
    private BimeBadaneRepository bimeBadaneRepository;

    @Autowired
    public BimeBadaneService(BimeBadaneRepository bimeBadaneRepository){
        this.bimeBadaneRepository = bimeBadaneRepository;
    }

    public List<BimeBadane> findAllBimeBadane() {
        return bimeBadaneRepository.findAll();
    }

    public void saveAllBimeBadane(List<BimeBadane> bimeBadaneList) {
        bimeBadaneRepository.saveAll(bimeBadaneList);
    }

    public void deleteAllBimeBadane() {
        bimeBadaneRepository.deleteAll();
    }

    public Optional<BimeBadane> findBimeBadane(Long id) {
        return bimeBadaneRepository.findById(id);
    }

    public BimeBadane saveBimeBadane(BimeBadane bimeBadane) {
        return bimeBadaneRepository.save(bimeBadane);
    }

    public void deleteBimeBadane(long bimeBadaneId){
        bimeBadaneRepository.deleteById(bimeBadaneId);
    }

    public BimeBadane updateBimeBadane(BimeBadane bimeBadane) throws InvocationTargetException, IllegalAccessException {
        BimeBadane existBimeBadane = findBimeBadane(bimeBadane.getId()).get();
        MyBeanCopy myBeanCopy = new MyBeanCopy();
        myBeanCopy.copyProperties(existBimeBadane, bimeBadane);
        return bimeBadaneRepository.save(existBimeBadane);
    }
}
