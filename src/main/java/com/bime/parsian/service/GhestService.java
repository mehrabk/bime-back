package com.bime.parsian.service;

import com.bime.parsian.beans.MyBeanCopy;
import com.bime.parsian.model.Ghest;
import com.bime.parsian.repository.GhestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;

@Service
public class GhestService {

    private GhestRepository ghestRepository;

    @Autowired
    public GhestService(GhestRepository ghestRepository){
        this.ghestRepository = ghestRepository;
    }


    public Ghest findGhest(Long id) {
        return ghestRepository.findById(id).get();
    }

    public Ghest saveGhest(Ghest ghest) {
        return ghestRepository.save(ghest);
    }

    public Ghest updateGhest(Ghest ghest) throws InvocationTargetException, IllegalAccessException {
        Ghest existGhest =  ghestRepository.findById(ghest.getId()).get();
        if (ghest.getImageUrl() == "") {
            existGhest.setImageUrl("");
        }
        MyBeanCopy myBeanCopy = new MyBeanCopy();
        myBeanCopy.copyProperties(existGhest, ghest);
        return ghestRepository.save(existGhest);
    }

    public void deleteGhest(Long ghestId) {
        ghestRepository.deleteById(ghestId);
    }
}
