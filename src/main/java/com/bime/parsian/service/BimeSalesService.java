package com.bime.parsian.service;

import com.bime.parsian.beans.MyBeanCopy;
import com.bime.parsian.model.BimeSales;
import com.bime.parsian.repository.BimeSalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@Service
public class BimeSalesService {

    private BimeSalesRepository bimeSalesRepository;

    @Autowired
    public BimeSalesService(BimeSalesRepository bimeSalesRepository){
        this.bimeSalesRepository = bimeSalesRepository;
    }

//    Basically getOne is a lazy load operation.
//    Thus you get only a reference (a proxy) to the entity.
//    That means no DB access is actually made.
//    Only when you call it's properties then it will query the DB. findByID does the call 'eagerly'/immediately when you call it, thus you have the actual entity fully populated
//
    public Optional<BimeSales> findBimeSales (Long id) {
        return bimeSalesRepository.findById(id);
    }

    public void deleteBimeSales(BimeSales bimeSales) {
        bimeSalesRepository.delete(bimeSales);
    }

    public BimeSales updateBimeSales(BimeSales bimeSales) throws InvocationTargetException, IllegalAccessException {
        BimeSales existBimeSales = findBimeSales(bimeSales.getId()).get();
        MyBeanCopy myBeanCopy = new MyBeanCopy();
        myBeanCopy.copyProperties(existBimeSales, bimeSales);
        return bimeSalesRepository.save(existBimeSales);
    }

    public BimeSales saveBimeSales(BimeSales bimeSales) {
        return bimeSalesRepository.save(bimeSales);
    }
}
