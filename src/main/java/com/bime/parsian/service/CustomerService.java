package com.bime.parsian.service;

import com.bime.parsian.beans.MyBeanCopy;
import com.bime.parsian.model.Customer;
import com.bime.parsian.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAllCustomers() {
        return this.customerRepository.findAll();
    }

    public List<Customer> saveAllCustomers(List<Customer> customerList) {
        return customerRepository.saveAll(customerList);
    }

    public Optional<Customer> findCustomer(long id){
        return customerRepository.findById(id);
    }

    public Customer saveCustomer (Customer customer) {
        return customerRepository.save(customer);
    }

    public void deleteAllCustomers() {
        customerRepository.deleteAll();
    }

    public Customer updateCustomer(Customer customer) throws InvocationTargetException, IllegalAccessException {
        Customer existCustomer = findCustomer((customer.getId())).get();
        MyBeanCopy myBeanCopy = new MyBeanCopy();
        myBeanCopy.copyProperties(existCustomer, customer);
        return customerRepository.save(existCustomer);
    }

    public void deleteCustomer(Long id){
        customerRepository.deleteById(id);
    }

    public List<Customer> findByUserName(String userName){
        return customerRepository.findAllByUserNameContaining(userName);
    }
}
