package com.bime.parsian.service;
import com.bime.parsian.exception.StorageException;
import com.bime.parsian.repository.StorageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class StorageService implements StorageRepository {
    private static final Logger logger = LoggerFactory.getLogger(StorageService.class);

    @Value("${com.resource.location}")
    private String rootLoc;

    @Override
    public void store(MultipartFile file, String fileName) throws IOException {
        try {
            if(file.isEmpty()) {
                throw new StorageException("Failed To Store Empty File " + fileName);
            }
            if(fileName.contains("..")) {
                // This is a security check
                throw new StorageException("Cannot store file with relative path outside current directory " + fileName);
            }
            try (InputStream inputStream = file.getInputStream()){
                Path rootLocation = Paths.get(rootLoc + "/upload");
                Files.copy(inputStream, rootLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + fileName, e);
        }
    }
}

