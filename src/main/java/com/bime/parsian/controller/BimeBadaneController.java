package com.bime.parsian.controller;

import com.bime.parsian.model.Ghest;
import com.bime.parsian.model.BimeBadane;
import com.bime.parsian.payload.response.MessageResponse;
import com.bime.parsian.service.BimeBadaneService;
import com.bime.parsian.service.CustomerService;
import com.bime.parsian.service.GhestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;


@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api/bimeBadane")
public class BimeBadaneController {

    private CustomerService customerService;
    private BimeBadaneService bimeBadaneService;
    private GhestService ghestService;

    @Autowired
    public BimeBadaneController(CustomerService customerService, BimeBadaneService bimeBadaneService, GhestService ghestService) {
        this.customerService = customerService;
        this.bimeBadaneService = bimeBadaneService;
        this.ghestService = ghestService;
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{bimeBadaneId}/addGhest", method = RequestMethod.POST)
    public Ghest bimeBadaneAddGhest(@PathVariable("bimeBadaneId") Long bimeBadaneId, @RequestBody Ghest newGhest) throws InvocationTargetException, IllegalAccessException {
        BimeBadane bimeBadane = bimeBadaneService.findBimeBadane(bimeBadaneId).get();
        newGhest.setBimeBadane(bimeBadane);
        return ghestService.saveGhest(newGhest);
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{bimeBadaneId}/updateGhest", method = RequestMethod.PUT)
    public Ghest bimeBadaneUpdateGhest(@PathVariable("bimeBadaneId") long bimeBadaneId, @RequestBody Ghest updateGhest) throws InvocationTargetException, IllegalAccessException {
        return ghestService.updateGhest(updateGhest);
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public BimeBadane updateBimeBadane(@RequestBody BimeBadane bimeBadane) throws InvocationTargetException, IllegalAccessException {
        return bimeBadaneService.updateBimeBadane(bimeBadane);
    }

    @GetMapping(value = "/getAll")
    public List<BimeBadane> getAllBimeBadaneList(@RequestParam("CId") long customerId) {
        List<BimeBadane> bimeBadaneList = customerService.findCustomer(customerId).get().getBimeBadaneList();

        return bimeBadaneList;
    }

    @PreAuthorize(" hasRole('ADMIN') ")
    @DeleteMapping(value = "/{bimeBadaneId}/delete")
    public void DeleteBimeBadane(@PathVariable("bimeBadaneId") long bimeBadaneId) {
        bimeBadaneService.deleteBimeBadane(bimeBadaneId);
    }

    @GetMapping(value = "/info")
    public BimeBadane getBimeBadane(@RequestParam("id") long bimeBadaneId) {
        return bimeBadaneService.findBimeBadane(bimeBadaneId).get();
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteBimeBadane(@RequestParam("id") long id) {
        try {
            bimeBadaneService.deleteBimeBadane(id);
            return ResponseEntity.ok("1");
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body(new MessageResponse("0"));
        }
    }
}


// this step in not true!!
