package com.bime.parsian.controller;

import com.bime.parsian.service.GhestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ghest")
public class GhestController {

    private GhestService ghestService;

    @Autowired
    public GhestController(GhestService ghestService) {
        this.ghestService = ghestService;
    }

    @PreAuthorize(" hasRole('ADMIN') ")
    @DeleteMapping(value = "/{ghestId}/delete")
    public void deleteGhest(@PathVariable("ghestId") Long ghestId) {
        ghestService.deleteGhest(ghestId);
    }
}
