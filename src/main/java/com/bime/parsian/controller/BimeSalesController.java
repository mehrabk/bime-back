package com.bime.parsian.controller;

import com.bime.parsian.model.Customer;
import com.bime.parsian.model.Ghest;
import com.bime.parsian.model.BimeSales;
import com.bime.parsian.payload.response.MessageResponse;
import com.bime.parsian.service.BimeSalesService;
import com.bime.parsian.service.CustomerService;
import com.bime.parsian.service.GhestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping(value = "/api/bimeSales")
public class BimeSalesController {

    private BimeSalesService bimeSalesService;
    private GhestService ghestService;
    private CustomerService customerService;

    @Autowired
    public BimeSalesController(BimeSalesService bimeSalesService, GhestService ghestService, CustomerService customerService) {
        this.bimeSalesService = bimeSalesService;
        this.ghestService = ghestService;
        this.customerService = customerService;
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{bimeSalesId}/addGhest", method = RequestMethod.POST)
    public Ghest bimeSalesAddGhest(@PathVariable("bimeSalesId") Long bimeSalesId, @RequestBody Ghest newGhest) throws InvocationTargetException, IllegalAccessException {
        BimeSales bimeSales = bimeSalesService.findBimeSales(bimeSalesId).get();
        newGhest.setBimeSales(bimeSales);
        return ghestService.saveGhest(newGhest);
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{bimeSalesId}/updateGhest", method = RequestMethod.PUT)
    public Ghest bimeSalesUpdateGhest(@RequestBody Ghest updatGhest) throws InvocationTargetException, IllegalAccessException {
        return ghestService.updateGhest(updatGhest);
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public BimeSales updateBimeSale(@RequestBody BimeSales bimeSales) throws InvocationTargetException, IllegalAccessException {
        return bimeSalesService.updateBimeSales(bimeSales);
    }

    @GetMapping(value = "/getAll")
    public List<BimeSales> getAllBimeSalesList(@RequestParam("CId") long customerId) {
//        if(customerService.findCustomer(customerId).isPresent() && !customerService.findCustomer(customerId).get().getBimeSalesList().isEmpty()){
            List<BimeSales> bimeSalesList = customerService.findCustomer(customerId).get().getBimeSalesList();
            return bimeSalesList;
//        }
    }

    @GetMapping(value = "/info")
    public BimeSales getBimeSales(@RequestParam("id") long id) {
        return bimeSalesService.findBimeSales(id).get();
    }



    // doroste FK tooie BimeSales Has Vali az Customer HAmi Mishe BimeSals Ro get kard.
    // mishod az inja ba bimeSalesId Ham delete Kard vali chon samte UI yekam ezafe kari dasht inkaro anjam dadim
    @PreAuthorize(" hasRole('ADMIN') ")
    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteBimeSales(@RequestParam("id") long bimeSalesId) {
        try {
            BimeSales bimeSales = bimeSalesService.findBimeSales(bimeSalesId).get();
            bimeSalesService.deleteBimeSales(bimeSales);
            return ResponseEntity.ok("1");
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body("0");
        }
    }
}
