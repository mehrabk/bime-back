package com.bime.parsian.controller;

import com.bime.parsian.payload.fileResponsePayload;
import com.bime.parsian.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api")
public class MediaController {
    private static final Logger logger = LoggerFactory.getLogger(MediaController.class);

    private StorageService storageService;

    @Autowired
    public MediaController(StorageService storageService){
        this.storageService = storageService;
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public fileResponsePayload uploadFile(@RequestParam MultipartFile file) throws Exception {
        logger.info(String.format("File name ==> '%s' uploaded successfully.", file.getOriginalFilename()));
        String originalName = file.getOriginalFilename().replaceAll(" ", "_");
        int a = originalName.lastIndexOf('.');
        String fileType = originalName.substring(a + 1);
        if (fileType.equalsIgnoreCase("jpg") || fileType.equalsIgnoreCase("jpeg")
                || fileType.equalsIgnoreCase("png") || fileType.equalsIgnoreCase("mp4")
                || fileType.equalsIgnoreCase("mov")){
            String fileName = UUID.randomUUID() + "." + fileType;
            storageService.store(file, fileName);
            return new fileResponsePayload(fileName);
        }else {
            throw new Exception("NOT SUPPORTED");
        }
    }

}
