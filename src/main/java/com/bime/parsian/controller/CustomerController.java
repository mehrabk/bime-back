package com.bime.parsian.controller;

import com.bime.parsian.model.BimeBadane;
import com.bime.parsian.model.BimeSales;
import com.bime.parsian.model.BimeType;
import com.bime.parsian.model.Customer;
import com.bime.parsian.payload.response.MessageResponse;
import com.bime.parsian.service.BimeBadaneService;
import com.bime.parsian.service.BimeSalesService;
import com.bime.parsian.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private CustomerService customerService;
    private BimeSalesService bimeSalesService;
    private BimeBadaneService bimeBadaneService;

    @Autowired
    public CustomerController(CustomerService customerService, BimeSalesService bimeSalesService, BimeBadaneService bimeBadaneService) {
        this.customerService = customerService;
        this.bimeSalesService = bimeSalesService;
        this.bimeBadaneService = bimeBadaneService;
    }

    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN') ")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<Customer> getAllCustomers() {
        return customerService.findAllCustomers();
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER') ")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Customer getCustomer(@RequestParam("id") Long customerId) {
        return customerService.findCustomer(customerId).get();
    }

//    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
//    @RequestMapping(value = "/update", method = RequestMethod.PUT)
//    public Customer updateCustomer(@RequestBody Customer customer) throws InvocationTargetException, IllegalAccessException {
//        return customerService.updateCustomer(customer);
//    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Customer saveCustomer(@RequestBody Customer customer) throws InvocationTargetException, IllegalAccessException {
        if(customer.getId() == 0 ) {
            return customerService.saveCustomer(customer);
        }else {
            return customerService.updateCustomer(customer);
        }
    }

    @PreAuthorize(" hasRole('ADMIN') ")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCustomer(@RequestParam("id") Long id) {
        try {
            customerService.deleteCustomer(id);
            return ResponseEntity.ok("1");
        }catch (Exception exception) {
            return ResponseEntity.badRequest().body(new MessageResponse("0"));
        }
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{customerId}/bimeSalesContract", method = RequestMethod.POST)
    public BimeSales bimeSalesContract(@PathVariable("customerId") long customerId, @RequestBody BimeSales bimeSalesContract) throws InvocationTargetException, IllegalAccessException {
        //we must handle when id not exist
        if (bimeSalesContract.getId() == 0) {
            bimeSalesContract.setType(BimeType.getType(1));
            Customer customer = customerService.findCustomer(customerId).get();
            bimeSalesContract.setCustomer(customer);
            return bimeSalesService.saveBimeSales(bimeSalesContract);
        } else {
            bimeSalesContract.setType(BimeType.getType(1));
             return bimeSalesService.updateBimeSales(bimeSalesContract);
        }
    }

    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') ")
    @RequestMapping(value = "/{customerId}/bimeBadaneContract", method = RequestMethod.POST)
    public BimeBadane bimeBadaneContract(@PathVariable("customerId") long customerId, @RequestBody BimeBadane bimeBadaneContract) throws InvocationTargetException, IllegalAccessException {
        if(bimeBadaneContract.getId() == 0) {
            bimeBadaneContract.setType(BimeType.getType(2));
            Customer customer = customerService.findCustomer(customerId).get();
            bimeBadaneContract.setCustomer(customer);
            return bimeBadaneService.saveBimeBadane(bimeBadaneContract);
        } else {
            bimeBadaneContract.setType(BimeType.getType(2));
            return bimeBadaneService.updateBimeBadane(bimeBadaneContract);
        }
    }

//    @PreAuthorize(" hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER') ")
//    @RequestMapping(value = "/search/byUserName/{userName}", method = RequestMethod.GET)
//    public List<Customer> findByUserName(@PathVariable("userName") String userName) {
//        return customerService.findByUserName(userName);
//    }

}


//        BimeType.getType(1);