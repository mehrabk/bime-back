package com.bime.parsian.payload;

import lombok.Getter;
import lombok.Setter;

public class fileResponsePayload {

    public fileResponsePayload(String fileName) {
        this.fileName = fileName;
    }

    @Setter
    @Getter
    private String fileName;

}
