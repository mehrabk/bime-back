package com.bime.parsian.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

public class UserInfoResponse {

    @Setter
    @Getter
    private String username;

    @Setter
    @Getter
    private Set<String> roles;

    public UserInfoResponse(String username, Set<String> roles){
        this.username = username;
        this.roles = roles;
    }
}
