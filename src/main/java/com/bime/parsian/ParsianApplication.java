package com.bime.parsian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParsianApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParsianApplication.class, args);
    }

}
