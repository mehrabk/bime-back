package com.bime.parsian.mockData;

import com.bime.parsian.model.Customer;
import com.bime.parsian.service.CustomerService;
import com.bime.parsian.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MockData implements CommandLineRunner {

    private CustomerService customerService;
    private UserService userService;

    @Autowired
    public MockData (CustomerService customerService, UserService userService) {
        this.customerService = customerService;
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {

//        customerService.deleteAllCustomers();
//
        Customer customer1 = new Customer("Mehrab", "Kor", "Bandar Turkman", "09039119333", "9555555");
        Customer customer2 = new Customer("Aram", "Kor", "Gomishan", "0911157570001", "6666666");
        Customer customer3 = new Customer("Nafis", "Esf", "Gomishan", "0911157570001", "6666666");

        List<Customer> customers = Arrays.asList(customer1, customer2, customer3);
        this.customerService.saveAllCustomers(customers);
    }
}
